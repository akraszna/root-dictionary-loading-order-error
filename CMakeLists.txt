# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Set up the project.
cmake_minimum_required( VERSION 3.6 )
project( ROOTDictLoadOrderError VERSION 1.0.0 LANGUAGES C CXX )

# Turn off the building of unit tests.
option( ATLAS_ALWAYS_BUILD_TESTS "Always build unit tests" FALSE )

# Set up the AtlasCMake and AtlasLCG modules.
set( AtlasCMake_DIR "${CMAKE_SOURCE_DIR}/atlasexternals/Build/AtlasCMake"
   CACHE PATH "Location of AtlasCMake" )
set( LCG_DIR "${CMAKE_SOURCE_DIR}/atlasexternals/Build/AtlasLCG"
   CACHE PATH "Location of AtlasLCG" )
set( LCG_VERSION_NUMBER 97 CACHE STRING "LCG version number to use" )
set( LCG_VERSION_POSTFIX "a_ATLAS_1" CACHE STRING "LCG version postfix" )
find_package( AtlasCMake REQUIRED )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED )

# Build the code in XAOD_ANALYSIS mode, I.e. with reduced Athena dependencies.
set( XAOD_ANALYSIS TRUE CACHE BOOL "Build the code in 'Analysis mode'" )
if( XAOD_ANALYSIS )
   add_definitions( -DXAOD_ANALYSIS )
endif()

# Set up the use of CTest for the project.
atlas_ctest_setup()

# Set up where to find the xAODUtilities CMake code.
set( xAODUtilities_DIR
   "${CMAKE_SOURCE_DIR}/athena/Event/xAOD/xAODCore/cmake"
   CACHE PATH "Directory holding the xAODUtilities module" )

# Extra dependencies for the runtime environment.
find_package( VDT )

# Set up the ATLAS project.
atlas_project()

# Set up an environment setup script for the project.
lcg_generate_env( SH_FILE "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh" )
install( FILES "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh"
   DESTINATION . )

# Set up the use of CPack for the project.
atlas_cpack_setup()
