//
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
//

// ROOT include(s).
#include <TClass.h>

// System include(s).
#undef NDEBUG
#include <cassert>

int main() {

   // Access the dictionary of both types that we (would) need.
   assert( TClass::GetClass( "xAOD::PhotonContainer" ) != nullptr );
   assert( TClass::GetClass( "xAOD::TrigPhotonContainer" ) != nullptr );

   // Return gracefully.
   return 0;
}
