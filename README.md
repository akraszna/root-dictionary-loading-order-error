# ROOT Dictionary Loading Order Error

Code demonstrating an issue that we see in ATLAS with ROOT 6.20/06's
dictionaries.

To build the code, you need to have access to
[LCG_97a_ATLAS_1](http://lcginfo.cern.ch/release/97a_ATLAS_1/). I.e. you need
to have access to [CVMFS](https://cernvm.cern.ch/portal/filesystem), and
be on a [CentOS 7](https://www.centos.org/) machine with
[GCC 8](https://gcc.gnu.org/gcc-8/) set up.

As long as all of those are set, you can build the project just like any other
[CMake](https://cmake.org/) project...

```sh
mkdir build
cd build/
cmake -DCMAKE_BUILD_TYPE=<YOUR_CHOICE> ../root-dictionary-loading-order-error/
make
```

Then, to set up the built binaries for your runtime environment, you need to
source the `setup.sh` script put into the build directory. Like:

```sh
source ./build/x86_64-centos7-gcc8-*/setup.sh
```

The error being demonstrated then shows up by trying to load both the
`xAODEgammaDict` and `xAODTrigEgammaDict` dictionary libraries one after the
other. It can not be done with ROOT 6.20/06. As doing so fails like:

```
[bash][atlas]:root-dict-error > python
Python 2.7.16 (default, Jun 13 2019, 18:32:39)
[GCC 8.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import ROOT
>>> t1 = ROOT.xAOD.TrigPhotonContainer
>>> t2 = ROOT.xAOD.PhotonContainer
In file included from libxAODEgammaDict dictionary payload:66:
In file included from /data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/xAODEgamma/PhotonContainer.h:13:
In file included from /data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/xAODEgamma/versions/PhotonContainer_v1.h:17:
/data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/xAODEgamma/PhotonContainerFwd.h:15:1: error: explicit specialization of 'DataVectorBase<xAOD::Photon_v1>' after instantiation
DATAVECTOR_BASE_FWD( xAOD::Photon, xAOD::Egamma );
^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/AthContainers/DataVector.h:650:20: note: expanded from macro 'DATAVECTOR_BASE_FWD'
template <> struct DataVectorBase<T>      \
                   ^~~~~~~~~~~~~~~~~
/data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/AthContainers/DataVector.h:783:42: note: implicit instantiation first required here
template <class T, class BASE = typename DataVectorBase<T>::Base>
                                         ^
...
```

And:

```
[bash][atlas]:root-dict-error > python
Python 2.7.16 (default, Jun 13 2019, 18:32:39)
[GCC 8.2.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import ROOT
>>> t1 = ROOT.xAOD.PhotonContainer
>>> t1 = ROOT.xAOD.TrigPhotonContainer
In file included from libxAODTrigEgammaDict dictionary payload:50:
In file included from /data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/xAODTrigEgamma/TrigPhotonContainer.h:13:
/data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/xAODTrigEgamma/versions/TrigPhotonContainer_v1.h:18:1: error: explicit specialization of 'DataVectorBase<xAOD::TrigPhoton_v1>' after instantiation
DATAVECTOR_BASE( xAOD::TrigPhoton_v1, xAOD::IParticle );
^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
/data-nvme1/projects/root-dict-error/build/x86_64-centos7-gcc8-dbg/include/AthContainers/DataVector.h:640:43: note: expanded from macro 'DATAVECTOR_BASE'
#define DATAVECTOR_BASE(T, BASE)          \
                                          ^
...
```
