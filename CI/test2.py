#!/usr/bin/env python
#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#

import ROOT
t1 = ROOT.xAOD.TrigPhotonContainer
t2 = ROOT.xAOD.PhotonContainer
